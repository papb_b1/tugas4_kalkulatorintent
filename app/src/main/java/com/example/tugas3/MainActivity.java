package com.example.tugas3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.button.MaterialButton;
import org.mozilla.javascript.Context;
import org.mozilla.javascript . Scriptable;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {

    TextView resultTextView, solutionTextView;
    MaterialButton buttonC, buttonKurungBuka, buttonKurungTutup;
    MaterialButton buttonBagi, buttonKali, buttonTambah, buttonKurang, buttonSamaDengan;
    MaterialButton button0, button1, button2, button3, button4, button5, button6, button7, button8, button9;
    MaterialButton buttonAC, buttonDot;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        resultTextView = findViewById(R.id.result_tv);
        solutionTextView = findViewById(R.id.solution_tv);

        for (int i : new int[]{R.id.button_c, R.id.button_open_bracket, R.id.button_close_bracket, R.id.button_divide, R.id.button_multiply, R.id.button_plus, R.id.button_minus, R.id.button_equals, R.id.button_0, R.id.button_1, R.id.button_2, R.id.button_3, R.id.button_4, R.id.button_5, R.id.button_6, R.id.button_7, R.id.button_8, R.id.button_9, R.id.button_ac, R.id.button_dot}) {
            assignId(i);
        }
    }

    void assignId(int id) {
        MaterialButton btn = findViewById(id);
        btn.setOnClickListener(this);
    }

    @Override
    public void onClick(View view) {
        MaterialButton button = (MaterialButton) view;
        String buttonText = button.getText().toString();
        String dataToCalculate = solutionTextView.getText().toString();

        if (buttonText.equals("AC")) {
            solutionTextView.setText("");
            resultTextView.setText("0");
            return;
        }
        if (buttonText.equals("=")) {
            solutionTextView.setText(resultTextView.getText());
//      Pindah halaman dengan intent
            Intent intent = new Intent(this, MainActivity2.class);
            intent.putExtra("result", resultTextView.getText());
            startActivity(intent);
            return;
        }
        if (buttonText.equals("C")) {
            dataToCalculate = dataToCalculate.substring(0, dataToCalculate.length() - 1);
        } else {
            dataToCalculate = dataToCalculate + buttonText;
        }
        solutionTextView.setText(dataToCalculate);

        String finalResult = getResult(dataToCalculate);

        if (!finalResult.equals("Err")) {
            resultTextView.setText(finalResult);
        }
    }

    String getResult(String data) {


        try {
            Context context = Context.enter();
            context.setOptimizationLevel(-1);
            Scriptable scriptable = context.initStandardObjects();
            String finalResult = context.evaluateString(scriptable, data, "Javascript", 1, null).toString();
            if (finalResult.endsWith(".0")) {
                finalResult = finalResult.replace(".0", "");
            }
            return finalResult;
        } catch (Exception e) {
            return "Err";
        }

    }
}